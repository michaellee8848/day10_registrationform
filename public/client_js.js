var ValidationApp = angular.module("ValidationApp", []);

(function(){
    var ValidationCtrl;

    ValidationCtrl = function($http){
        var ctrl = this;

        ctrl.firstName = "";
        ctrl.lastName = "";
        ctrl.postalCode = "";
        ctrl.email = "";
        ctrl.address = "";
        ctrl.country = "";
        ctrl.contactNumber = "";
        ctrl.dateOfBirth = "";
        ctrl.password = "";

        // response message
        ctrl.status = {
            message: "",
            code: 0
        };

        ctrl.register = function() {
            $http.post("/register",{
                params : {
                    firstName : ctrl.firstName,
                    lastName  : ctrl.lastName,
                    postalCode : ctrl.postalCode,
                    email : ctrl.email,
                    address : ctrl.address,
                    country : ctrl.country,
                    contactNumber : ctrl.contactNumber,
                    dateOfBirth : ctrl.dateOfBirth,
                    password: ctrl.password
                }
            }).
            // (function getAge(dateOfBirth) {
            //     var now = new Date();
            //     function isLeap(year) {
            //         return (((year%4)==0 && ((year %100) !=0) || ((year % 400) ==0)));
            //     }
            //     var days = Math.floor((now.getTime() - dateOfBirth.getTime())/1000/60/60/24);
            //     var age = 0;
            //     for (var y = dateOfBirth.getFullYear(); y <= now.getFullYear(); y++) {
            //         var daysInYear = isLeap(y) ? 366 : 365;
            //         if (days >= daysInYear) {
            //             days -= daysInYear;
            //             age ++;
            //         }
            //     }
            //     return age;
            // });
            // if (age < 18) {
            //     ctrl.status.message = "You are under 18";
            // }

            then(function(){
                console.info(" Success");
                ctrl.status.message ="Your registration is complete.";
                ctrl.status.code = 202;
                $window.location = "/thankyou";
            }).catch(function(){
                console.info(" Error");
                ctrl.status.message = "Your registration failed.";
                ctrl.status.code = 400;
            });
        };
    };

    ValidationApp.controller("ValidationCtrl", ['$http', ValidationCtrl]);
})
();
