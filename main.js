var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));

app.post("/register", function(req, res){
    var firstName = req.body.params.firstName;
    var lastName = req.body.params.lastName;
    var postalCode = req.body.params.postalCode;
    var email = req.body.params.email;
    var address = req.body.params.address;
    var contactNumber = req.body.params.contactNumber;
    var gender = req.body.params.gender;
    var country = req.body.params.country;
    var dateOfBirth = req.body.params.dateOfBirth;
    var password = req.body.params.password;
    
    console.info("First Name : %s", firstName);
    console.info("Last Name: %s", lastName);
    console.info("Postal Code %s", postalCode);
    console.info("Email address %s", email) ;
    console.info("Address %s", address) ;
    console.info("Contact Number %s", contactNumber) ;
    console.info("Gender %s", gender) ;
    console.info("Country %s", country) ;
    console.info("Date of Birth %s", dateOfBirth) ;
    console.info("Password %s", password);
    res.status(200).end();
});

app.get("/thankyou", function(req, res) {
    res.redirect("thankyou.html");
});

var portNumber = process.argv[2] || 3000;
console.info(process.argv[2]);

app.listen(parseInt(portNumber), function(){
    console.info("Webserver started on port 3000");
});